package com.rinat.spring.rest.ExceptionHandling;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class EmployeeIncorrectData {
    private String info;
}
