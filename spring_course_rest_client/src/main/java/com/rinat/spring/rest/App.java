package com.rinat.spring.rest;

import com.rinat.spring.rest.Configuration.MyConfig;
import com.rinat.spring.rest.entity.Employee;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MyConfig.class);
        Communication communication = context.getBean("communication", Communication.class);

//        List<Employee> allEmployees = communication.getAllEmployees();
//        allEmployees.forEach(System.out::println);

//        Employee empById = communication.getEmployee(1);
//        System.out.println(empById);

//        Employee employee = new Employee("Sveta", "Sokolova", "IT", 1200);
//        employee.setId(7);
//        communication.saveEmployee(employee);

        communication.deleteEmployee(7);

    }
}
